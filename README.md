# nordea-mvn-2019-slides

Slides written in markdown for Nordea 2019 Maven training, rendered using [Hugo](https://gohugo.io) static page generator.

Hosted at GitLab pages: https://nordea-maven-2019.gitlab.io/nordea-mvn-2019-slides/