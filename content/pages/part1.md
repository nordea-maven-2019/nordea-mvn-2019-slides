---
title: "Part 1: Maven Introduction"
date: 2019-10-07
summary: Usage of Maven in terminal, understanding pom structure, clean and default lifecycle and default plugin bindings
---

Here we'll start learning basic maven interactions with hands-on exercies.

## Prework: Setting up git repostiory and maven configuration

1. On your local machine create a folder that will be used to hold all the training work, let's call it `mvn2019`.

2. Place file `ci-settings.xml` that was distributed with intro email inside training folder:

3. Inside training folder create empty git repository named `nordea-mvn2019-single` with command `git init nordea-mvn2019-single`

4. Add following `.gitignore` rules to the created repository and commit them
<details>
  <summary><code>.gitignore</code> file contents</summary>

```
### IntelliJ IDEA ###
.idea/
*.iws
*.iml
*.ipr

### Maven ###
**/target
```
</details>

**SOLUTION:** [Prework Solution](https://gitlab.com/nordea-maven-2019/nordea-mvn-2019-single/-/tags/part1-prework-solution)

## Exercise 1: Generating project from Archetype

**NOTE: You'll need to use supplied ci_settings.xml file to complete this step**

In training folder call the following command:

```
> mvn -s ci_settings.xml archetype:generate
```

In interactive dialog use following options, you can confirm **DEFAULT** by just pressing `Enter`:

**WARNING:** It can be a bit slow on Nordea machines, please wait a bit for interactive prompt when message `[INFO] Generating project in Interactive mode` appears in output

* `Choose a number or apply filter (format: [groupId:]artifactId, case sensitive contains): 20:` **DEFAULT**
* `Choose org.apache.maven.archetypes:maven-archetype-quickstart version: 4:` **DEFAULT**
* `Define value for property 'groupId':` **com.nordea.mvn2019**
* `Define value for property 'artifactId':` **nordea-mvn-2019-basic**
* `Define value for property 'version' 1.0-SNAPSHOT: :` **DEFAULT**
* `Define value for property 'package' com.nordea.mvn2019: :` **DEFAULT**

Generated result should be the folder named `nordea-mvn-2019-basic` containing quickstart maven project

**SOLUTION:** [Exercise 1 Solution](https://gitlab.com/nordea-maven-2019/nordea-mvn-2019-single/-/tags/part1-ex1-solution)

## Exercise 2: Building the project in terminal

Here we'll start learning about project lifecycle and maven goals

In training folder issue following commands:

```
> cd nordea-mvn-2019-basic
> mvn -s ../ci_settings.xml clean install site
```

What was done here is invocation of 3 consecutive maven goals: `clean`, `install`, `site`.

You should see text outputs from plugin executions and at the end `[INFO] BUILD SUCCESS` printed.

## Exercise 3: Importing Maven project into IntelliJ IDE

1. Open IntelliJ IDE on your laptop and from menu bar select:  
   `File` -> `New` -> `Project from Existing Sources...`

2. In Directory selection prompt select folder with created quickstart project `nordea-mvn-2019-basic`

3. When model selection dialog appears select:  
   `Import Project from External Model` and pick `Maven`

4. In project settings keep all defaults and click `Next`

5. In `Select Maven projects to import` keep the default and click `Next`

6. In `Select Project SDK` pick or add and then pick JDK that is installed in your machine.  
   **NOTE**: If you're having problems in this step, please ask for help quickly

7. In Project name and folder selection dialog keep defaults and click `Next`

## Lecture: Discussing pom.xml structure

Here we'll discuss what actually is contained inside `pom.xml` file

<details>
  <summary>Example of minimal working <code>pom.xml</code> file</summary>

```xml
<project>
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.mycompany.app</groupId>
  <artifactId>my-app</artifactId>
  <version>1</version>
</project>
```
</details>

* `<properties>` section  
* `<dependencies>` section
* `<build>` section

## Exercise 4: Set Java source/target version

1. Add a lambda function in main method of App class
2. Change property `<maven.compiler.target>` version to 12 and rebuild project
3. Change both `<maven.compiler.target>` and `<maven.compiler.source>` to value `1.8`

**SOLUTION:** [Exercise 4 and 5 Solution](https://gitlab.com/nordea-maven-2019/nordea-mvn-2019-single/-/tags/part1-ex4-ex5-solution)

## Lecture: Wider perspective - what else comes into play

* [`default` Lifecycle bindings](https://maven.apache.org/ref/3.6.2/maven-core/default-bindings.html)
* [Super POM](https://maven.apache.org/guides/introduction/introduction-to-the-pom.html)
* [Central Repository](https://repo.maven.apache.org/maven2/) and [Maven Search](https://search.maven.org/)

## Exercise 5: Fixing plugin versions at latest

Here we'll do some hands-on seraches into Maven Central to figure out latest versions of plugins for use in our project.

1. For each plugin specified under `<build> <pluginManagement> <plugins>` look it up in [Maven Search](https://search.maven.org/) and set it to newest version
2. **OPTIONAL** For each updated version extract it as a property in `<properites>` section by highlighting it in editor and using `Alt + Ctrl + V` key shortcut of IntelliJ or just right click and right click and `Refactor` -> `Extract` -> `Property` from menu.

**GOOD PRACTICE:** It's best to have plugins and dependencies versions extracted to properties within pom to have one summary of used versions, but it's not essential in training.

**SOLUTION:** [Exercise 4 and 5 Solution](https://gitlab.com/nordea-maven-2019/nordea-mvn-2019-single/-/tags/part1-ex4-ex5-solution)

## Exercise 6: Configuring Plugins and creating runnable jar

Each maven plugin can be supplied with additional configuration to change its default behavior by adding `<configuration>` section to plugin definition.

<details>
  <summary>Add following configuration section to <code>`maven-complier-plugin`</code></summary>

```xml
<configuration>
  <compilerArgument>-Xlint:all</compilerArgument>
  <showWarnings>true</showWarnings>
  <showDeprecation>true</showDeprecation>
</configuration>
```
</details>

<details>
  <summary>Add following configuration section to <code>`maven-jar-plugin`</code></summary>

```xml
<configuration>
  <archive>
    <manifest>
      <mainClass>com.nordea.mvn2019.App</mainClass>
    </manifest>
  </archive>
</configuration>
```
</details>

Verify that changes work correctly by running `mvn verify` and then starting our newely created runnable jar with command:

```
> java -jar /target/nordea-mvn-2019-single-1.0-SNAPSHOT.jar
```

You should see `Hello World!` text message in console output.

**SOLUTION:** [Exercise 6 Solution](https://gitlab.com/nordea-maven-2019/nordea-mvn-2019-single/-/tags/part1-ex6-solution)

## Exercise 7: Adding REPL logic

Currently we have a simple maven project that allows us to build a runnable jar that prints `Hello World!` and that's all. Not great, not terrible.

Let's improve that a little bit by adding a basic REPL logic. REPL stands for Read-Evaluate-Print-Loop which a type of user interaction typical to most terminal applications.

Replace `main` method in `App` class with new contents and add import for `Scanner` class.

<details>
  <summary><code>main</code> method contents</summary>

```java
  public static void main( String[] args ) {
    System.out.println("Nordea 2019 Maven Training - Simple Calculator in Java");
    Scanner in = new Scanner(System.in);

    while (true) {
      System.out.print("Calc > ");
      if (in.hasNextLine()) {
        String input = in.nextLine();
        if (input.toLowerCase().equals("exit")) {
          break;
        } else {
          System.out.println(input);
        }
      } else {
        System.out.println();
        break;
      }
    }
  }
```
</details>

<details>
  <summary<code>Scanner</code> import (in imports section)</summary>

```java
import java.util.Scanner;
```
</details>

**SOLUTION:** [Exercise 7 Solution](https://gitlab.com/nordea-maven-2019/nordea-mvn-2019-single/-/tags/part1-ex7-solution)