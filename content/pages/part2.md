---
title: "Part 2: Maven in CI Scenario and Artifact Distribution"
date: 2019-10-07
summary: distribution managment and fully functional runnable jar
---

Here we'll consider typical challanges and solutions that arise when using Maven for building Java projects in Continous Integration environment.

## Exercise 1: Pom Section `distributionManagment`

In previous part we've managed to build a runnable jar, now let's configure `distributionManagment` so that we can upload it to repostiory.

1. Configure target repositories in `pom.xml` by adding `<distributionManagement>` section

<details>
  <summary>Repositories config in <code>&lt;distributionManagment&gt;</code> section</summary>

```xml
  <distributionManagement>
    <repository>
      <id>internal</id>
      <url>http://localhost:8080/repository/internal/</url>
    </repository>
    <snapshotRepository>
      <id>snapshots</id>
      <url>http://localhost:8080/repository/snapshots/</url>
    </snapshotRepository>
  </distributionManagement>
```
</details>

2. Let's try uploading our artifact with command
```
> mvn clean deploy
```

And it doesn't work, giving us `HTTP 401 Unauthorized`, so we need to additionally configure server credentials in `ci-settings.xml`

3. Configure `<servers>` section with credentials to our Apache Archiva user

**DANGER:** It's security breach to just commit credentials in code repository and to keep credentials in plain text in config files. Here we do it for simplicity, but it shouldn't be handled this way in production setup.

<details>
  <summary>Credentials config in <code>&lt;servers&gt;</code> section</summary>

```xml
<servers>
    <server>
      <id>internal</id>
      <username>mvn2019</username>
      <password>mvn2019</password>
    </server>
    <server>
      <id>snapshots</id>
      <username>mvn2019</username>
      <password>mvn2019</password>
    </server>
  </servers>
```
</details>

Now `deploy` should be succesful, so let's invoke maven again with updated settings.

```
> mvn -s ci-settings.xml clean deploy
```

**SOLUTION:** [Exercise 1 Solution](https://gitlab.com/nordea-maven-2019/nordea-mvn-2019-single/-/tags/part2-ex1-solution)

## Exercise 2: Improve Application Logic - Add arithmetic expressions grammar

Our goal is to have interactive calculator, so let's diverge a bit from strict maven focus into expanding application logic a bit.

We'll use [ANTLR4](https://www.antlr.org) parser generator to create code from a simple grammar for arithmetic expressions.

In our project folder create following directory structure `src/main/antlr4/com/nordea/mvn2019` and inside this folder create file `Calc.g4` with following contents.

<details>
  <summary><code>Calc.g4</code> contents</summary>

```antlr
grammar Calc;

/* Tokens (terminal) */
POW: '^';
MUL: '*';
DIV: '/';
ADD: '+';
SUB: '-';
NUM: [0-9]+;
DOUBLE : NUM '.' NUM;
WHITESPACE: [ \r\n\t]+ -> skip;

/* Rules (non-terminal) */
start : expr;

expr
   : NUM                          # Number
   | DOUBLE                       # Double
   | '(' inner=expr ')'           # Parentheses
   | left=expr op=POW right=expr  # Power
   | left=expr op=MUL right=expr  # Multiplication
   | left=expr op=DIV right=expr  # Division
   | left=expr op=ADD right=expr  # Addition
   | left=expr op=SUB right=expr  # Subtraction
   ;
```
</details>

In order to actually have something done with this grammar file we need to add plugins that will convert this file into generated code.

So let's add following sections to `pom.xml`

1. Dependency for antlr4 code in compilation, so generated code can be compiled.

<details>
<summary><code>Antlr4</code> dependency</summary>

```xml
<dependency>
    <groupId>org.antlr</groupId>
    <artifactId>antlr4</artifactId>
    <version>4.7.2</version>
    <scope>compile</scope>
</dependency>
```
</details>

2. Plugin binding and configuration for `antlr4-maven-plugin` in `<pluginManagment>`

<details>
<summary><code>Antlr4</code> maven plugin in <code>&lt;pluginManagment&gt;</code></summary>

```xml
<plugin>
    <groupId>org.antlr</groupId>
    <artifactId>antlr4-maven-plugin</artifactId>
    <version>4.7.2</version>
    <configuration>
        <arguments>
            <argument>-visitor</argument>
        </arguments>
    </configuration>
    <executions>
        <execution>
            <id>antlr</id>
            <phase>generate-sources</phase>
            <goals>
                <goal>antlr4</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```
</details>

3. Actual plugin invocation in `<plugins>` section

<details>
<summary><code>Antlr4</code> maven plugin in <code>&lt;pluginManagment&gt;</code></summary>

```xml
<plugins>
    <plugin>
        <groupId>org.antlr</groupId>
        <artifactId>antlr4-maven-plugin</artifactId>
    </plugin>
</plugins>
```
</details>

**SOLUTION:** [Exercise 2 Solution](https://gitlab.com/nordea-maven-2019/nordea-mvn-2019-single/-/tags/part2-ex2-solution)

## Exercise 3: Improve Application Logic - Add implementation of grammar semantics

Right now we have created grammar and generated code from that grammar, now we need to add semantics (meaning) to our grammar. We'll do that by creating an implementation of `CalcBaseVisitor`. Let's create a java class `CalcVisitorImpl` in same package as class `App` with following contents.

<details>
<summary><code>CalcVisitorImpl</code> java code</summary>

```java
package com.nordea.mvn2019;

import static com.nordea.mvn2019.CalcParser.*;

public class CalcVisitorImpl extends CalcBaseVisitor<Double> {

  @Override
  public Double visitMultiplication(MultiplicationContext ctx) {
    return this.visit(ctx.left) * this.visit(ctx.right);
  }

  @Override
  public Double visitAddition(AdditionContext ctx) {
    return this.visit(ctx.left) + this.visit(ctx.right);
  }

  @Override
  public Double visitSubtraction(SubtractionContext ctx) {
    return this.visit(ctx.left) - this.visit(ctx.right);
  }

  @Override
  public Double visitNumber(NumberContext ctx) {
    return Double.parseDouble(ctx.NUM().getText());
  }

  @Override
  public Double visitDouble(DoubleContext ctx) {
    return Double.parseDouble(ctx.DOUBLE().getText());
  }

  @Override
  public Double visitDivision(DivisionContext ctx) {
    Double right = this.visit(ctx.right);
    if (right == 0.0D) {
      throw new IllegalArgumentException("Division by 0!");
    }

    Double left = this.visit(ctx.left);

    return left / right;
  }

  @Override
  public Double visitParentheses(ParenthesesContext ctx) {
    return this.visit(ctx.inner);
  }

  @Override
  public Double visitPower(PowerContext ctx) {
    return Math.pow(this.visit(ctx.left), this.visit(ctx.right));
  }
}
```
</details>

**SOLUTION:** [Exercise 3 Solution](https://gitlab.com/nordea-maven-2019/nordea-mvn-2019-single/-/tags/part2-ex3-solution)

## Exercise 4: Improve Application Logic - Add tests for created grammar semantics

Next step is to add JUnit5 style tests for our implementation. In order to use JUnit5 we need to change our test dependency from older version and add actual test implementation. After that `maven-surefire-plugin` will take care of running the tests in build.

1. Let's start with updating `Junit` dependecy, switch from `JUnit4` to `JUnit5`

<details>
<summary>Updated <code>JUnit</code> dependency and property definition for <code>&lt;properties&gt;</code> section</summary>

```xml
<dependency>
    <groupId>org.junit.jupiter</groupId>
    <artifactId>junit-jupiter</artifactId>
    <version>${junit-jupiter.version}</version>
    <scope>test</scope>
</dependency>
```

```xml
<junit-jupiter.version>5.5.1</junit-jupiter.version>
```
</details>

2. Then let's add following test class `CalcVisitorImplTest` to our test package

<details>
<summary><code>CalcVisitorImplTest</code> source code</summary>

```java
package com.nordea.mvn2019;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.stream.Stream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class CalcVisitorImplTest {

  CalcVisitorImpl calc = new CalcVisitorImpl();

  @ParameterizedTest
  @MethodSource("argumentsProvider")
  void testExpressions(String expression, double expected) {
    CharStream inputStream = CharStreams.fromString(expression);

    Lexer lexer = new CalcLexer(inputStream);
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    CalcParser parser = new CalcParser(tokens);
    ParseTree tree = parser.start();

    double actual = calc.visit(tree);

    assertEquals(expected, actual);
  }

  @Test
  void testZeroDivision() {
    CharStream inputStream = CharStreams.fromString("1/0");

    Lexer lexer = new CalcLexer(inputStream);
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    CalcParser parser = new CalcParser(tokens);
    ParseTree tree = parser.start();

    Assertions.assertThrows(IllegalArgumentException.class, () -> {
      calc.visit(tree);
    });
  }

  static Stream<Arguments> argumentsProvider() {
    return Stream.of(
        arguments("10",   10.0),
        arguments("1 + 1", 2.0),
        arguments("4 - 5", -1.0),
        arguments("2 * 2", 4.0),
        arguments("8 / 2", 4.0),
        arguments("2 ^ 2", 4.0),
        arguments("((2) + 2)", 4.0),
        arguments("1.25", 1.25),
        arguments("1.25 + 1.25", 2.5),
        arguments("4.5 - 5.5", -1.0),
        arguments("2.5 * 2.5", 6.25),
        arguments("12.5 / 2.5", 5.0),
        arguments("2.5 ^ 2", 6.25),
        arguments("((2.75) + 2.75)", 5.5)
    );
  }
}
```
</details>

3. Remove no longer needed `AppTest` class from source code

4. Verify that it all works correctly by invoking

```
> mvn clean verify
```

You should see information on tests being run in output from surefire plugin

**SOLUTION:** [Exercise 4 Solution](https://gitlab.com/nordea-maven-2019/nordea-mvn-2019-single/-/tags/part2-ex4-solution)

## Exercise 5: Improve Application Logic - Putting it all together

Right now we have a runnable jar that contains a simple REPL logic that only echoes input to terminal and we have working parser for arithmetic expressions. Let's put it together into working solution by updating main method.

Inside `main` method in `App` class introduce following changes

1. Add creation of `CalcVisitorImpl` instance before `while` loop

<details>
<summary><code>CalcVisitorImpl</code> instance creation</summary>

```java
CalcVisitorImpl calc = new CalcVisitorImpl();
```
</details>

2. Replace `else` clause of inner `if` block checking if `input` was equal to `exit` with following contents. Now instead of doing echo of input, we'll pass it to parser to evaluate arithmetic expression.

<details>
<summary><code>else</code> clause contents</summary>

```java
CharStream inputStream = CharStreams.fromString(input);
Lexer lexer = new CalcLexer(inputStream);
CommonTokenStream tokens = new CommonTokenStream(lexer);
CalcParser parser = new CalcParser(tokens);
ParseTree tree = parser.start();

try {
    Double result = calc.visit(tree);
    System.out.println(result);
} catch (IllegalArgumentException ex) {
    System.out.println(ex.getMessage());
}
```
</details>

3. Remember to update imports in `App` class, IntelliJ should take care of that automatically, but in any case here's the reference how imports should be updated.

<details>
<summary>New imports to be included in <code>App</code></summary>

```java
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.tree.ParseTree;
```
</details>

Let's build jar again and try running it.

```
> mvn clean verify
> java -jar .\target\nordea-mvn-2019-single-1.0-SNAPSHOT.jar
```

**SOLUTION:** [Exercise 5 Solution](https://gitlab.com/nordea-maven-2019/nordea-mvn-2019-single/-/tags/part2-ex5-solution)

## Exercise 6: Final product - making runnable jar with dependencies

Since our runnable jar is missing code at runtime we need to create it other way and for that we'll `maven-assembly-plugin`

1. Let's start by adding plugin config in `pom.xml` `<pluginManagement>` and plugin invocation in `plugins` section.

<details>
<summary><code>maven-assembly-plugin</code> config</summary>

```xml
<plugin>
  <groupId>org.apache.maven.plugins</groupId>
  <artifactId>maven-assembly-plugin</artifactId>
  <executions>
    <execution>
      <phase>package</phase>
      <goals>
        <goal>single</goal>
      </goals>
      <configuration>
        <archive>
          <manifest>
            <mainClass>com.nordea.mvn2019.App</mainClass>
          </manifest>
        </archive>
        <descriptorRefs>
          <descriptorRef>jar-with-dependencies</descriptorRef>
        </descriptorRefs>
        <appendAssemblyId>false</appendAssemblyId>
      </configuration>
    </execution>
  </executions>
</plugin>
```
</details>

<details>
<summary><code>maven-assembly-plugin</code> invocation</summary>

```xml
<plugin>
  <groupId>org.apache.maven.plugins</groupId>
  <artifactId>maven-assembly-plugin</artifactId>
</plugin>
```
</details>

2. Next step will be to disable normal `.jar` file creation from `maven-jar-plugin` we'll acheive that by binding it's default goal to phase `none`.

<details>
<summary><code>maven-jar-plugin</code> updated config</summary>

```xml
<plugin>
  <artifactId>maven-jar-plugin</artifactId>
  <version>3.1.2</version>
  <executions>
    <execution>
      <id>default-jar</id>
      <!-- bind to none phase to skip plugin execution -->
      <phase>none</phase>
    </execution>
  </executions>
</plugin>
```
</details>

3. Last step will be to actually add antlr runtime dependency in depencencies section

<details>
<summary><code>antlr4-runtime</code> dependency for runtime</summary>

```xml
<dependency>
  <groupId>org.antlr</groupId>
  <artifactId>antlr4-runtime</artifactId>
  <version>4.7.2</version>
</dependency>
```
</details>

4. Let's verify that it all works by building package again and testing our calc at runtime.

```
> mvn clean verify
> java -jar ./target/nordea-mvn-2019-single-1.0-SNAPSHOT.jar
```

Inside calc it should be possible to do following interaction

```
Nordea 2019 Maven Training - Simple Calculator in Java
Calc > 1 + 1.25 - 55
-52.75
Calc > exit
```

**SOLUTION:** [Exercise 6 Solution](https://gitlab.com/nordea-maven-2019/nordea-mvn-2019-single/-/tags/part2-ex6-solution)


## Exercise 7: Finalising release with RELEASE version

1. Final step will be to set our version to RELEASE version. We'll do that by just removing `SNAPSHOT` part from version string.

<details>
<summary>New content for <code>&lt;version&gt;</code> tag</summary>

```xml
<version>1.0</version>
```
</details>

2. Deploy artifact to release repository with deploy goal, remember to include `ci-settings.xml` as it contains our server credentials.

```
> mvn -s ci-settings.xml clean verify deploy
```

3. Go to Archiva instance and download new artifact from release repostiory, verify that uploaded artifact behaves correctly.