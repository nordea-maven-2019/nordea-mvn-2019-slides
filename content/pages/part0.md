---
title: "Part 0: Verify Environment Setup"
date: 2019-10-07
summary: Environment verification before actual training
---

## Intro

In this section we'll proceed with verifying environment setup to confirm that everything is in place.

To begin start a terminal application on your laptop, on Windows that will be `Powershell` or `Cmd`.

For simplicity create a folder on your machine to hold all training work, call it `mvn2019`.

## Step 1: Verify that Java Development Kit 8 is installed

Run following command in terminal

```
> javac -version
```

You should see output similar to this

```
javac 1.8.0_212-3-redhat
```

**NOTE: Version 8 is required for some of training steps to work**

## Step 2: Verify that Maven is installed

Run following command in terminal

```
> mvn -version
```

You should see output similar to this

```
Apache Maven 3.6.1 (d66c9c0b3152b2e69ee9bac180bb8fcc8e6af555; 2019-04-04T21:00:29+02:00)
Maven home: /home/wd/tools/apache-maven-3.6.1
Java version: 11.0.4, vendor: Oracle Corporation, runtime: /usr/lib64/jvm/java-11-openjdk-11
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "4.12.14-lp151.28.16-default", arch: "amd64", family: "unix"
```

## Step 3: Verify that Git is installed

Run following command in terminal

```
> git --version
```

You should see output similar to this

```
git version 2.16.4
```

## Step 4: Verify that IntelliJ IDEA is installed

1. Start `IntelliJ` from desktop or start menu

2. If there's a need to configure license server, assistance will be provided

## Step 5: Start and configure Apache Archiva in terminal

1. Unpack Apache Archiva in a training folder

2. In terminal go to Apache Archiva folder and into `bin` folder

```
> cd C:\tools\apache-archiva-2.2.4\bin
```

3. Inside `bin` folder invoke command

```
> .\archiva console
```

4. Inside webbrowser go to `http://localhost:8080`

5. Click red button `CREATE ADMIN USER`

6. In form set password to `admin1` and select `Validated` checkbox

7. In left menu go to `Manage` in `Users` section, then select `Add` in `Users` listing

8. Set parameters for new user  
   * Username = `mvn2019`
   * Password = `mvn2019`

9. After user creation click edit button on new user and grant it additional Roles
   * Repository Manager on `internal`
   * Repository Manager on `snapshots`

10. Log out of Admin user and log in with new `mvn2019` to verify that it's setup correctly. In particular you should see `Upload Artifact` in left menu.